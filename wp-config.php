<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_liliyavorski' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[>F4j=lc^lwWq{90t{u]M&SjU;O3nKmmyx)Wf4@,V$G1P4r#$Tf1B[z2g_r0s{fL' );
define( 'SECURE_AUTH_KEY',  '9tgE5oMto}4fb|<+(NgI$5VJi}&9>6IsLdk/~GmPe8VgVr1.$}|;EkLct>No|w_Q' );
define( 'LOGGED_IN_KEY',    'Q[p.;,>Kcc0!>1%juN(O,7nD;s4 Qj|xjLH-)WI4,P.p?uX)sW<Y-ynq8Ch6/ 0R' );
define( 'NONCE_KEY',        '*OO9G;ZL(45@IfU1z`ki/dMcuWEbKMsC[qtXLb}L2L;yzZP}B+2Bvv!op[VKpDux' );
define( 'AUTH_SALT',        'O(5A$XDk6;2IG=(cQb#t</w(hg,D3ww/qSaOW|BmT<7.JXR/t#0cAq!MdVlq)Uap' );
define( 'SECURE_AUTH_SALT', 'GSNt>v1HoQsJ28pW1~ufmbj`6v;P5.Rjc/?_oW)BQ+?<@hR38#70UbA~LhC_DZ2a' );
define( 'LOGGED_IN_SALT',   '<olZf6y$0>G^+L 2gtOzmE.xRbnno_HPQtr~+b7*i>02;$o(=-p^a+g:7Z`C|iD7' );
define( 'NONCE_SALT',       'r3xiL9Uf_2sB1,Vmfy~]g3<vK=pN(rQ^Aqc<?2.RR`11so|uv`jfpYu> q~wC:G-' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
