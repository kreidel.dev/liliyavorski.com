<?php get_sidebar(); ?>
</section>
    <footer id="footer" role="contentinfo">
        <div id="copyright" style="text-align:center; margin-top:40px; font-weight:400; font-size:12px">
            &copy; <?php echo esc_html( date_i18n( __( 'Y', 'liliyavorski' ) ) ); ?> <?php echo esc_html( get_bloginfo( 'name' ) ); ?>
        </div>
    </footer>
<?php wp_footer(); ?>
</body>
</html>