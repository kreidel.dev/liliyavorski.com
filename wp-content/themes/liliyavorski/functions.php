<?php
add_action( 'after_setup_theme', 'liliyavorski_setup' );
function liliyavorski_setup() {
	load_theme_textdomain( 'liliyavorski', get_template_directory() . '/languages' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'responsive-embeds' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'html5', array( 'search-form' ) );
	global $content_width;
	if ( !isset( $content_width ) ) { $content_width = 1920; }
	register_nav_menus( array( 'main-menu' => esc_html__( 'Main Menu', 'liliyavorski' ) ) );
}

add_action( 'wp_enqueue_scripts', 'liliyavorski_enqueue' );
function liliyavorski_enqueue() {
	wp_enqueue_style( 'normalize', get_stylesheet_uri() );
	wp_enqueue_style( 'liliyavorski-style', get_template_directory_uri() . '/css/styles.min.css' );

	wp_enqueue_script( 'liliyavorski-sctipt', get_template_directory_uri() . '/js/scripts.js', array(), '1.0.0', true );
}


add_filter( 'document_title_separator', 'liliyavorski_document_title_separator' );
function liliyavorski_document_title_separator( $sep ) {
	$sep = '|';
	return $sep;
}

add_filter( 'the_title', 'liliyavorski_title' );
function liliyavorski_title( $title ) {
	if ( $title == '' ) {
		return '...';
	} else {
		return $title;
	}
}


// add_filter( 'nav_menu_link_attributes', 'liliyavorski_schema_url', 10 );
// function liliyavorski_schema_url( $atts ) {
// 	$atts['itemprop'] = 'url';
// 	return $atts;
// }

// if ( !function_exists( 'liliyavorski_wp_body_open' ) ) {
// 	function liliyavorski_wp_body_open() {
// 		do_action( 'wp_body_open' );
// 	}
// }

// add_action( 'wp_body_open', 'liliyavorski_skip_link', 5 );
// function liliyavorski_skip_link() {
// 	echo '<a href="#content" class="skip-link screen-reader-text">' . esc_html__( 'Skip to the content', 'liliyavorski' ) . '</a>';
// }

// add_filter( 'the_content_more_link', 'liliyavorski_read_more_link' );
// function liliyavorski_read_more_link() {
// 	if ( !is_admin() ) {
// 		return ' <a href="' . esc_url( get_permalink() ) . '" class="more-link">' . sprintf( __( '...%s', 'liliyavorski' ), '<span class="screen-reader-text">  ' . esc_html( get_the_title() ) . '</span>' ) . '</a>';
// 	}
// }

// add_filter( 'excerpt_more', 'liliyavorski_excerpt_read_more_link' );
// function liliyavorski_excerpt_read_more_link( $more ) {
// 	if ( !is_admin() ) {
// 		global $post;
// 		return ' <a href="' . esc_url( get_permalink( $post->ID ) ) . '" class="more-link">' . sprintf( __( '...%s', 'liliyavorski' ), '<span class="screen-reader-text">  ' . esc_html( get_the_title() ) . '</span>' ) . '</a>';
// 	}
// }

// add_filter( 'big_image_size_threshold', '__return_false' );

// add_filter( 'intermediate_image_sizes_advanced', 'liliyavorski_image_insert_override' );
// function liliyavorski_image_insert_override( $sizes ) {
// 	unset( $sizes['medium_large'] );
// 	unset( $sizes['1536x1536'] );
// 	unset( $sizes['2048x2048'] );
// 	return $sizes;
// }

// add_action( 'widgets_init', 'liliyavorski_widgets_init' );
// function liliyavorski_widgets_init() {
// 	register_sidebar( array(
// 		'name' => esc_html__( 'Sidebar Widget Area', 'liliyavorski' ),
// 		'id' => 'primary-widget-area',
// 		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
// 		'after_widget' => '</li>',
// 		'before_title' => '<h3 class="widget-title">',
// 		'after_title' => '</h3>',
// 	) );
// }

// add_action( 'wp_head', 'liliyavorski_pingback_header' );
// function liliyavorski_pingback_header() {
// 	if ( is_singular() && pings_open() ) {
// 		printf( '<link rel="pingback" href="%s" />' . "\n", esc_url( get_bloginfo( 'pingback_url' ) ) );
// 	}
// }

// add_action( 'comment_form_before', 'liliyavorski_enqueue_comment_reply_script' );
// function liliyavorski_enqueue_comment_reply_script() {
// 	if ( get_option( 'thread_comments' ) ) {
// 		wp_enqueue_script( 'comment-reply' );
// 	}
// }

// add_filter( 'get_comments_number', 'liliyavorski_comment_count', 0 );
// function liliyavorski_comment_count( $count ) {
// 	if ( !is_admin() ) {
// 		global $id;
// 		$get_comments = get_comments( 'status=approve&post_id=' . $id );
// 		$comments_by_type = separate_comments( $get_comments );
// 		return count( $comments_by_type['comment'] );
// 	} else {
// 		return $count;
// 	}
// }


/* Disable WordPress Admin Bar for all users */
//add_filter( 'show_admin_bar', '__return_false' );



// Remove Actions
remove_action('wp_head','feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action('wp_head','feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head','rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head','wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head','wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head','rel_canonical' );
remove_action('wp_head','wp_shortlink_wp_head', 10, 0 );
remove_action('wp_head','start_post_rel_link');
remove_action('wp_head','index_rel_link');
remove_action('wp_head','parent_post_rel_link', 10, 0);
remove_action('wp_head','adjacent_posts_rel_link_wp_head',10,0); // for WordPress >= 3.0
add_action('do_feed','wpb_disable_feed', 1);
add_action('do_feed_rdf','wpb_disable_feed', 1);
add_action('do_feed_rss','wpb_disable_feed', 1);
add_action('do_feed_rss2','wpb_disable_feed', 1);
add_action('do_feed_atom','wpb_disable_feed', 1);
add_action('do_feed_rss2_comments','wpb_disable_feed', 1);
add_action('do_feed_atom_comments','wpb_disable_feed', 1);